# wcd

Wherever Change Directory (WCD) 16-bit version for DOS, similar to Norton Change Directory (NCD) with more features.

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## WCD.LSM

<table>
<tr><td>title</td><td>wcd</td></tr>
<tr><td>version</td><td>6.0.5</td></tr>
<tr><td>entered&nbsp;date</td><td>2023-04-23</td></tr>
<tr><td>description</td><td>Wherever Change Directory</td></tr>
<tr><td>keywords</td><td>chdir, cd, change directory, fast</td></tr>
<tr><td>author</td><td>waterlan _AT_ xs4all.nl (Erwin Waterlander)</td></tr>
<tr><td>maintained&nbsp;by</td><td>waterlan _AT_ xs4all.nl (Erwin Waterlander)</td></tr>
<tr><td>primary&nbsp;site</td><td>https://waterlan.home.xs4all.nl/wcd.html</td></tr>
<tr><td>previous&nbsp;site</td><td>http://waterlan.home.xs4all.nl/</td></tr>
<tr><td>alternate&nbsp;site</td><td>http://sourceforge.net/projects/wcd/</td></tr>
<tr><td>original&nbsp;site</td><td>http://www.xs4all.nl/~waterlan/</td></tr>
<tr><td>platforms</td><td>DOS, Windows, Unix</td></tr>
<tr><td>copying&nbsp;policy</td><td>[GNU General Public License, Version 2](LICENSE)</td></tr>
<tr><td>summary</td><td>Wherever Change Directory (WCD) 16-bit version for DOS, similar to Norton Change Directory (NCD) with more features.</td></tr>
</table>
