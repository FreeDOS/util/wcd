# Danish translation wcd.
# Copyright (C) 2017 Erwin Waterlander (msgids)
# This file is distributed under the same license as the wcd package.
# Joe Hansen <joedalton2@yahoo.dk>, 2017.
#
msgid ""
msgstr ""
"Project-Id-Version: wcd-6.0.0-beta4\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-12-12 20:29+0100\n"
"PO-Revision-Date: 2017-02-11 22:30+0200\n"
"Last-Translator: Joe Hansen <joedalton2@yahoo.dk>\n"
"Language-Team: Danish <dansk@dansk-gruppen.dk>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Bugs: Report translation errors to the Language-Team address.\n"

#: wcd.c:170
#, c-format
msgid "Wcd: error: "
msgstr "Wcd: fejl: "

#: wcd.c:183 wcd.c:237 wcd.c:257
#, c-format
msgid "Unable to read file %s: %s\n"
msgstr "Kan ikke læse filen %s: %s\n"

#: wcd.c:246
#, c-format
msgid "Unable to read file %s: Not a regular file.\n"
msgstr "Kan ikke læse filen %s: Ikke en normal fil.\n"

#: wcd.c:259 wcd.c:346
#, c-format
msgid "Unable to write file %s: %s\n"
msgstr "Kan ikke skrive filen %s: %s\n"

#: wcd.c:348
#, c-format
msgid "Unable to close file %s: %s\n"
msgstr "Kan ikke lukke filen %s: %s\n"

#: wcd.c:409 wcd.c:414 wfixpath.c:65
#, c-format
msgid "Memory allocation error in %s: %s\n"
msgstr "Hukommelsesallokeringsfejl i %s: %s\n"

#: wcd.c:716 wcd.c:730
#, c-format
msgid "%s added to file %s\n"
msgstr "%s tilføjet til filen %s\n"

#: wcd.c:765 wcd.c:1097
#, c-format
msgid "%s is not a directory.\n"
msgstr "%s er ikke en mappe.\n"

#: wcd.c:770
#, c-format
msgid "Please wait. Scanning disk. Building treedata-file %s from %s\n"
msgstr "Vent venligst. Skanner disk. Bygger treedata-filen %s fra %s\n"

#: wcd.c:832 wcd.c:2224 wcd.c:2247 wcd.c:2327
#, c-format
msgid "Writing file \"%s\"\n"
msgstr "Skriver filen »%s«\n"

#: wcd.c:837
msgid "Write access to tree-file denied.\n"
msgstr "Skriveadgang til tree-file nægtet.\n"

#: wcd.c:838
msgid "Set TEMP environment variable if this is a read-only disk.\n"
msgstr "Angiv TEMP-miljøvaribalen hvis dette er en skrivebeskyttet disk.\n"

#: wcd.c:967
#, c-format
msgid "Removed symbolic link %s\n"
msgstr "Fjernede symbolsk henvisning %s\n"

#: wcd.c:971
#, c-format
msgid "Unable to remove symbolic link %s: %s\n"
msgstr "Kan ikke fjerne symbolsk henvinsing %s: %s\n"

#: wcd.c:975
#, c-format
msgid "%s is a symbolic link to a file.\n"
msgstr "%s er en symbolsk henvisning til en fil.\n"

#: wcd.c:1058
#, c-format
msgid "Recursively remove %s? Are you sure? y/n :"
msgstr "Rekursivt fjern %s? Er du sikker? j/n :"

#: wcd.c:1080 wcd.c:1090
#, c-format
msgid "Removed directory %s\n"
msgstr "Fjernede mappen %s\n"

#: wcd.c:1122 wcd.c:1191 wcd.c:1259
#, c-format
msgid "line too long in %s ( > %d). The treefile could be corrupt, else fix by increasing WCD_MAXPATH in source code.\n"
msgstr "linjen er for lang i %s ( > %d). Treefilen kan være ødelagt, ellers ret ved at øge WCD_MAXPATH i kildekoden.\n"

#: wcd.c:1123 wcd.c:1192 wcd.c:1260
#, c-format
msgid "file: %s, line: %d,"
msgstr "fil: %s, linje: %d,"

#: wcd.c:1130 wcd.c:1199 wcd.c:1267
#, c-format
msgid " length: %d\n"
msgstr " længde: %d\n"

#: wcd.c:1768 wcd.c:1837
msgid "Alias file in UTF-16 format is not supported.\n"
msgstr "Aliasfil i UTF-16-format er ikke understøttet.\n"

#: wcd.c:1937
#, c-format
msgid ""
"Usage: wcd [options] [directory]\n"
"\n"
msgstr ""
"Brug: wcd [tilvalg] [mappe]\n"
"\n"

#: wcd.c:1938
#, c-format
msgid "directory:  Name of directory to change to.\n"
msgstr "mappe: Navn på mappe at ændre til.\n"

#: wcd.c:1939
#, c-format
msgid ""
"            Wildcards *, ? and [SET] are supported.\n"
"\n"
msgstr ""
"            Jokertegn *, ? og [SET] er understøttet.\n"
"\n"

#: wcd.c:1941
#, c-format
msgid "options:\n"
msgstr "tilvalg:\n"

#: wcd.c:1942
#, c-format
msgid "  -a                      Add current path to treefile\n"
msgstr "  -a                      Tilføj nuværende sti til treefile\n"

#: wcd.c:1943
#, c-format
msgid "  -aa                     Add current and all parent paths to treefile\n"
msgstr "  -aa                     Tilføj nuværende stier og alle overstier til treefile\n"

#: wcd.c:1944
#, c-format
msgid "  -A PATH                 Add tree from PATH\n"
msgstr "  -A STI                  Tilføj træ fra STI\n"

#: wcd.c:1945
#, c-format
msgid "  -b                      Ban current path\n"
msgstr "  -b                      Placer nuværende sti i karantæne\n"

#: wcd.c:1946
#, c-format
msgid "  -c,  --direct-cd        direct CD mode\n"
msgstr "  -c,  --direct-cd        direct cd-tilstand\n"

#: wcd.c:1947
#, c-format
msgid "  -d DRIVE                set DRIVE for stack & go files (DOS)\n"
msgstr "  -d DRIVE                angiv DREV for stack & go-filer (DOS)\n"

#: wcd.c:1948
#, c-format
msgid "  -e                      add current path to Extra treefile\n"
msgstr "  -e                      tilføj nuværende sti til Extra treefile\n"

#: wcd.c:1949
#, c-format
msgid "  -ee                     add current and all parent paths to Extra treefile\n"
msgstr "  -ee                     tilføj nuværende sti og alle overstier til Extra treefile\n"

#: wcd.c:1950
#, c-format
msgid "  -E PATH                 add tree from PATH to Extra treefile\n"
msgstr "  -E STI                  tilføj træ fra STI til Extra treefile\n"

#: wcd.c:1951
#, c-format
msgid "  -f FILE                 use extra treeFile\n"
msgstr "  -f FIL                  brug ekstra treeFile\n"

#: wcd.c:1952
#, c-format
msgid "  +f FILE                 add extra treeFile\n"
msgstr "  +f FIL                  tilføj ekstra treeFile\n"

#: wcd.c:1953
#, c-format
msgid "  -g                      Graphics\n"
msgstr "  -g                      Grafik\n"

#: wcd.c:1954
#, c-format
msgid "  -gd                     Graphics, dump tree\n"
msgstr "  -gd                     Grafik, dump træ\n"

#: wcd.c:1955
#, c-format
msgid "  -G PATH                 set PATH Go-script\n"
msgstr "  -G STI                  angiv STI Go-skript\n"

#: wcd.c:1956
#, c-format
msgid "  -GN, --no-go-script     No Go-script\n"
msgstr "  -GN, --no-go-script     Intet Go-skript\n"

#: wcd.c:1957
#, c-format
msgid "  -h,  --help             show this Help\n"
msgstr "  -h,  --help             vis denne hjælpetekst\n"

#: wcd.c:1960
#, c-format
msgid "  -i,  --ignore-case      Ignore case (default)\n"
msgstr "  -i,  --ignore-case      Ignorer store/små bogstaver (standard)\n"

#: wcd.c:1961
#, c-format
msgid "  +i,  --no-ignore-case   regard case\n"
msgstr "  +i,  --no-ignore-case   tag højde for store/små bogstaver\n"

#: wcd.c:1963
#, c-format
msgid "  -i,  --ignore-case      Ignore case\n"
msgstr "  -i,  --ignore-case      Ignorer store/små bogstaver\n"

#: wcd.c:1964
#, c-format
msgid "  +i,  --no-ignore-case   regard case (default)\n"
msgstr "  +i,  --no-ignore-case   tag højde for store/små bogstaver (standard)\n"

#: wcd.c:1967
#, c-format
msgid "  -I,  --ignore-diacritics     Ignore diacritics\n"
msgstr "  -I,  --ignore-diacritics     Ignorer diakritiske tegn\n"

#: wcd.c:1968
#, c-format
msgid "  +I,  --no-ignore-diacritics  regard diacritics (default)\n"
msgstr "  +I,  --no-ignore-diacritics  tag højde for diakritiske tegn (standard)\n"

#: wcd.c:1969
#, c-format
msgid "  -j,  --just-go          Just go mode\n"
msgstr "  -j,  --just-go          Just go-tilstand\n"

#: wcd.c:1970
#, c-format
msgid "  -k,  --keep-paths       Keep paths\n"
msgstr "  -k,  --keep-paths       Bevar stier\n"

#: wcd.c:1971
#, c-format
msgid "  -K,  --color            colors\n"
msgstr "  -K,  --color            farver\n"

#: wcd.c:1972
#, c-format
msgid "  -l ALIAS                aLias current directory\n"
msgstr "  -l ALIAS                aLias nuværende mappe\n"

#: wcd.c:1973
#, c-format
msgid "  -ls                     List the aliases\n"
msgstr "  -ls                     Vis aliasserne\n"

#: wcd.c:1974
#, c-format
msgid "  -L,  --license          show software License\n"
msgstr "  -L,  --license          vis programlicens\n"

#: wcd.c:1975
#, c-format
msgid "  -m DIR                  Make DIR, add to treefile\n"
msgstr "  -m MAPPE                Lav MAPPE, tilføj til treefile\n"

#: wcd.c:1976
#, c-format
msgid "  -M DIR                  Make DIR, add to extra treefile\n"
msgstr "  -M MAPPE                Lav MAPPE, tilføj til ekstra treefile\n"

#: wcd.c:1977
#, c-format
msgid "  -n PATH                 use relative treefile in PATH\n"
msgstr "  -n STI                  brug relativ treefile i STI\n"

#: wcd.c:1978
#, c-format
msgid "  +n PATH                 add relative treefile in PATH\n"
msgstr "  +n STI                  tilføj relativ treefile i STI\n"

#: wcd.c:1979
#, c-format
msgid "  -N,  --numbers          use Numbers\n"
msgstr "  -N,  --numbers          brug numre\n"

#: wcd.c:1980
#, c-format
msgid "  -o                      use stdOut\n"
msgstr "  -o                      brug standardud\n"

#: wcd.c:1981
#, c-format
msgid "  -od, --to-stdout        dump matches\n"
msgstr "  -od, --to-stdout        dump match\n"

#: wcd.c:1982
#, c-format
msgid "  -q,  --quiet            Quieter operation\n"
msgstr "  -q,  --quiet            Roligere operation\n"

#: wcd.c:1983
#, c-format
msgid "  -r DIR                  Remove DIR\n"
msgstr "  -r MAPPE                Fjern MAPPE\n"

#: wcd.c:1984
#, c-format
msgid "  -rmtree DIR             Remove DIR recursive\n"
msgstr "  -rmtree MAPPE           Fjern MAPPE rekursivt\n"

#: wcd.c:1985
#, c-format
msgid "  -s                      Scan disk from $HOME\n"
msgstr "  -s                      Skan disk fra $HOME\n"

#: wcd.c:1986
#, c-format
msgid "  -S PATH                 Scan disk from PATH\n"
msgstr "  -S STI                  Skan disk fra STI\n"

#: wcd.c:1987
#, c-format
msgid "  +S PATH                 Scan disk from PATH, create relative treefile\n"
msgstr "  +S STI                  Skan disk fra STI, opret relativ treefile\n"

#: wcd.c:1988
#, c-format
msgid "  -t                      don't strip /tmp_mnt from paths\n"
msgstr "  -t                      fjern ikke /tmp_mnt fra stier\n"

#: wcd.c:1989
#, c-format
msgid "  -T,  --ascii-tree       draw tree with ASCII characters\n"
msgstr "  -T,  --ascii-tree       tegn træ med ASCII-tegn\n"

#: wcd.c:1990
#, c-format
msgid "  -Ta, --alt-tree-nav     Alternative tree navigation\n"
msgstr "  -Ta, --alt-tree-nav     Alternativ trænavigation\n"

#: wcd.c:1991
#, c-format
msgid "  -TC, --center-tree      Centered tree view\n"
msgstr "  -TC, --center-tree      Centreret trævisning\n"

#: wcd.c:1992
#, c-format
msgid "  -Tc, --compact-tree     Compact tree\n"
msgstr "  -Tc, --compact-tree     Kompakt træ\n"

#: wcd.c:1993
#, c-format
msgid "  -Td, --cjk-width        support legacy CJK fonts\n"
msgstr "  -Td, --cjk-width        understøt forældet CJK-skrifttyper\n"

#: wcd.c:1994
#, c-format
msgid "  -u USER                 use USER's treefile\n"
msgstr "  -u BRUGER               anvend BRUGERs treefile\n"

#: wcd.c:1995
#, c-format
msgid "  +u USER                 add USER's treefile\n"
msgstr "  +u BRUGER               tilføj BRUGERs treefile\n"

#: wcd.c:1996
#, c-format
msgid "  -v,  --verbose          Verbose operation\n"
msgstr "  -v,  --verbose          Uddybende operation\n"

#: wcd.c:1997
#, c-format
msgid "  -V,  --version          show Version info\n"
msgstr "  -V,  --version          vis versionsinformation\n"

#: wcd.c:1998
#, c-format
msgid "  -w,  --wild-match-only  Wild matching only\n"
msgstr "  -w,  --wild-match-only  Kun jokertegns match\n"

#: wcd.c:1999
#, c-format
msgid "  -x PATH                 eXclude PATH during disk scan\n"
msgstr "  -x STI                  ekskluder STI under diskskanning\n"

#: wcd.c:2000
#, c-format
msgid "  -xf FILE                eXclude paths from FILE\n"
msgstr "  -xf FIL                 ekskluder stier fra FIL\n"

#: wcd.c:2001
#, c-format
msgid "  -y,  --assume-yes       assume Yes on all queries\n"
msgstr "  -y,  --assume-yes       antag Ja på alle forespørgsler\n"

#: wcd.c:2002
#, c-format
msgid "  -z NUMBER               set max stack siZe\n"
msgstr "  -z NUMMER               angiv maks. stakstørrelse\n"

#: wcd.c:2003
#, c-format
msgid "  -[NUMBER]               push dir (NUMBER times)\n"
msgstr "  -[ANTAL]                push mappe (ANTAL gange)\n"

#: wcd.c:2004
#, c-format
msgid "  +[NUMBER]               pop dir (NUMBER times)\n"
msgstr "  +[ANTAL]                pop mappe (ANTAL gange)\n"

#: wcd.c:2005
#, c-format
msgid "  =                       show stack\n"
msgstr "  =                       vis stak\n"

#: wcd.c:2014 wcd.c:2144
#, c-format
msgid "wcd %s (%s) - Wherever Change Directory\n"
msgstr "wcd %s (%s) - Wherever Change Directory\n"

#: wcd.c:2015
#, c-format
msgid ""
"Chdir for Dos and Unix.\n"
"\n"
msgstr ""
"Chdir for Dos og Unix.\n"
"\n"

#: wcd.c:2019
#, c-format
msgid "DOS 16 bit version (WATCOMC).\n"
msgstr "DOS-version 16-bit (WATCOMC).\n"

#: wcd.c:2021
#, c-format
msgid "DOS 16 bit version (TURBOC).\n"
msgstr "DOS-version 16-bit (TURBOC).\n"

#: wcd.c:2023
#, c-format
msgid "DOS 32 bit version (DJGPP).\n"
msgstr "DOS-version 32-bit (DJGPP).\n"

#: wcd.c:2025
#, c-format
msgid "DOS 32 bit version (WATCOMC).\n"
msgstr "DOS-version 32-bit (WATCOMC).\n"

#: wcd.c:2032
#, c-format
msgid "Win64 version (MSVC %d).\n"
msgstr "Win64-version (MSVC %d).\n"

#: wcd.c:2034
#, c-format
msgid "Win64 version (MinGW-w64).\n"
msgstr "Win64-version (MinGW-w64).\n"

#: wcd.c:2038
#, c-format
msgid "Win32 version (WATCOMC).\n"
msgstr "Win32-version (WATCOMC).\n"

#: wcd.c:2040
#, c-format
msgid "Win32 version (MSVC %d).\n"
msgstr "Win32-version (MSVC %d).\n"

#: wcd.c:2042
#, c-format
msgid "Win32 version (MinGW-w64).\n"
msgstr "Win32-version (MinGW-w64).\n"

#: wcd.c:2044
#, c-format
msgid "Win32 version (MinGW).\n"
msgstr "Win32-version (MinGW).\n"

#: wcd.c:2051
#, c-format
msgid "This version is for MSYS and WinZsh.\n"
msgstr "Denne version er for MSYS og WinZsh.\n"

#: wcd.c:2053
#, c-format
msgid "This version is for Windows PowerShell.\n"
msgstr "Denne version er for Windows PowerShell.\n"

#: wcd.c:2055
#, c-format
msgid "This version is for Windows Command Prompt (cmd.exe).\n"
msgstr "Denne version er for Windows Command Prompt (cmd.exe).\n"

#: wcd.c:2060
#, c-format
msgid "OS/2 version"
msgstr "OS2/2-verison"

#: wcd.c:2071
#, c-format
msgid "This version is for native MSYS.\n"
msgstr "Denne version er for MSYS.\n"

#: wcd.c:2074
#, c-format
msgid "Cygwin version.\n"
msgstr "Cygwin-version.\n"

#: wcd.c:2077
#, c-format
msgid "This version is for DJGPP DOS bash.\n"
msgstr "Denne version er for DJGPP DOS bash.\n"

#: wcd.c:2080
#, c-format
msgid "This version is for OS/2 bash.\n"
msgstr "Denne version er for OS/2 bash.\n"

#: wcd.c:2083
#, c-format
msgid "Interface: "
msgstr "Grænseflade: "

#: wcd.c:2089
#, c-format
msgid "ncurses version %s.%d\n"
msgstr "ncurses version %s.%d\n"

#: wcd.c:2092
#, c-format
msgid "PDCurses build %d\n"
msgstr "PDCurses kompilering %d\n"

#: wcd.c:2094
#, c-format
msgid "curses\n"
msgstr "curses\n"

#: wcd.c:2103
#, c-format
msgid "stdout\n"
msgstr "standardud\n"

#: wcd.c:2107
#, c-format
msgid "Native language support included.\n"
msgstr "Standardunderstøttelse for sprog er inkluderet.\n"

#: wcd.c:2108
#, c-format
msgid "LOCALEDIR=%s\n"
msgstr "LOCALEDIR=%s\n"

#: wcd.c:2110
#, c-format
msgid "No native language support included.\n"
msgstr "Ingen standardunderstøttelse for sprog er inkluderet.\n"

#: wcd.c:2113
#, c-format
msgid "Current locale uses CP%u encoding.\n"
msgstr "Nuværende sprog bruger CP%u-kodning.\n"

#: wcd.c:2115
#, c-format
msgid "Current locale uses %s encoding.\n"
msgstr "Nuværende sprog bruger %s-kodning.\n"

#: wcd.c:2118
#, c-format
msgid "With Unicode support.\n"
msgstr "Med Unicodeunderstøttelse.\n"

#: wcd.c:2123
#, c-format
msgid "  Euro symbol: "
msgstr "  Euro-symbol: "

#: wcd.c:2125
#, c-format
msgid "  Chinese characters: "
msgstr "  kinesiske tegn: "

#: wcd.c:2129
#, c-format
msgid "Without Unicode support.\n"
msgstr "Uden Unicodeunderstøttelse.\n"

#: wcd.c:2132
#, c-format
msgid "With Unicode normalization.\n"
msgstr "Med Unicodenormalisering.\n"

#: wcd.c:2134
#, c-format
msgid "Without Unicode normalization.\n"
msgstr "Uden Unicodenormalisering.\n"

#: wcd.c:2137
#, c-format
msgid "Download the latest executables and sources from:\n"
msgstr "Hent den seneste kørbare fil og kilder fra:\n"

#: wcd.c:2145
#, c-format
msgid ""
"Copyright (C) 1996-%d Erwin Waterlander\n"
"Copyright (C) 1994-2002 Ondrej Popp on C3PO\n"
"Copyright (C) 1995-1996 DJ Delorie on _fixpath()\n"
"Copyright (C) 1995-1996 Borja Etxebarria & Olivier Sirol on Ninux Czo Directory\n"
"Copyright (C) 1994-1996 Jason Mathews on DOSDIR\n"
"Copyright (C) 1990-1992 Mark Adler, Richard B. Wales, Jean-loup Gailly,\n"
"Kai Uwe Rommel and Igor Mandrichenko on recmatch()\n"
msgstr ""
"Ophavsret 1996-%d Erwin Waterlander\n"
"Ophavsret 1994-2002 Ondrej Popp on C3PO\n"
"Ophavsret 1995-1996 DJ Delorie for _fixpath()\n"
"Ophavsret 1995-1996 Borja Etxebarria & Olivier Sirol on Ninux Czo Directory\n"
"Ophavsret 1994-1996 Jason Mathews on DOSDIR\n"
"Ophavsret 1990-1992 Mark Adler, Richard B. Wales, Jean-loup Gailly,\n"
"Kai Uwe Rommel og Igor Mandrichenko for recmatch()\n"

#: wcd.c:2153
#, c-format
msgid ""
"Source code to scan Windows LAN was originally written and placed\n"
"in the public domain by Felix Kasza.\n"
"Markus Kuhn's free wcwidth() and wcswidth() implementations are used.\n"
"Rugxulo is the original author of query_con_codepage() (public domain).\n"
"\n"
msgstr ""
"Kildekode til at skanne Windows LAN blev oprindelig skrevet og\n"
"placeret i offentlig ejendom af Felix Kasza.\n"
"Markus Kuhns frie wcwidth()- og wcswidth()-implementeringer anvendes.\n"
"Rugxulo er den oprindelige forfatter af query_con_codepage()\n"
"(offentlige ejendom).\n"
"\n"

#: wcd.c:2159
#, c-format
msgid ""
"This program is free software; you can redistribute it and/or\n"
"modify it under the terms of the GNU General Public License\n"
"as published by the Free Software Foundation; either version 2\n"
"of the License, or (at your option) any later version.\n"
"\n"
msgstr ""
"Dette er et frit program; du kan videredistribuere det og/eller\n"
"ændre det under betingelserne i GNU General Public License\n"
"som udgivet af Free Software Foundation; enten version 2\n"
"af licensen, eller (efter dit valg) enhver senere version.\n"
"\n"

#: wcd.c:2165
#, c-format
msgid ""
"This program is distributed in the hope that it will be useful,\n"
"but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
"MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
"GNU General Public License for more details.\n"
"\n"
msgstr ""
"Dette program distribueres med håb om, at det vil være nyttigt,\n"
"men UDEN NOGEN FORM FOR GARANTI; også uden den underforståede\n"
"garanti for SALGBARHED eller EGNETHED FOR ET BESTEMT FORMÅL. Se\n"
"GNU General Public License for yderligere detaljer.\n"
"\n"

#: wcd.c:2171
#, c-format
msgid ""
"You should have received a copy of the GNU General Public License\n"
"along with this program; if not, write to the Free Software\n"
"Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.\n"
msgstr ""
"Du skal have modtaget en kopi af GNU General Public License\n"
"sammen med dette program; hvis ikke, så skriv til Free Software\n"
"Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,\n"
"MA 02110-1301 USA.\n"

#: wcd.c:2204
#, c-format
msgid "creating directory %s\n"
msgstr "opretter mappe %s\n"

#: wcd.c:2536 wcd.c:2576 wcd.c:2591 wcd.c:2659 wcd.c:2728 wcd.c:3230 wcd.c:3249
#, c-format
msgid "Value of environment variable %s is too long.\n"
msgstr "Værdien for miljøvariablen %s er for lang.\n"

#: wcd.c:2565
#, c-format
msgid "Failed to unset environment variable TERM: %s\n"
msgstr "Kunne ikke frakoble miljøvariablen TERM: %s\n"

#: wcd.c:2623 wcd.c:2652
msgid "Environment variable HOME or WCDHOME is not set.\n"
msgstr "Miljøvariablen HOME eller WCDHOME er ikke angivet.\n"

#: wcd.c:2835
#, c-format
msgid "aliasfile: %s\n"
msgstr "aliasfil: %s\n"

#: wcd.c:2849
msgid "HOME is not defined\n"
msgstr "HOME er ikke defineret\n"

#: wcd.c:2853
msgid "WCDHOME is not defined\n"
msgstr "WCDHOME er ikke defineret\n"

#: wcd.c:2857
msgid "WCDSCAN is not defined\n"
msgstr "WCDSCAN er ikke defineret\n"

#: wcd.c:2882
msgid "Graphics mode only supported in wcd with curses-based interface.\n"
msgstr "Grafiktilstand er kun understøttet i wcd med curses-baseret grænseflade.\n"

#: wcd.c:3163
#, c-format
msgid "%s added to aliasfile %s\n"
msgstr "%s tilføjet til aliasfilen %s\n"

#: wcd.c:3262
#, c-format
msgid "Unable to read file %s or %s\n"
msgstr "Kunne ikke læse filen %s eller %s\n"

#: wcd.c:3412
#, c-format
msgid "WCDSCAN directory {%s}\n"
msgstr "WCDSCAN-mappe {%s}\n"

#: wcd.c:3414
#, c-format
msgid "banning {%s}\n"
msgstr "placerer i karantæne {%s}\n"

#: wcd.c:3416
#, c-format
msgid "excluding {%s}\n"
msgstr "ekskluderer {%s}\n"

#: wcd.c:3418
#, c-format
msgid "filtering {%s}\n"
msgstr "filtrerer {%s}\n"

#: wcd.c:3678
#, c-format
msgid "No directory found matching %s\n"
msgstr "Der blev ikke fundet en mappe, der matcher %s\n"

#: wcd.c:3679
msgid "Perhaps you need to rescan the disk or path is banned.\n"
msgstr "Måske skal du skanne disken igen eller stien er måske placeret i karantæne.\n"

#: wcd.c:3808
#, c-format
msgid "Cannot change to %s\n"
msgstr "Kan ikke ændre til %s\n"

#: wcddir.c:109
#, c-format
msgid "%lu (\"%s\") reported by \"%s\".\n"
msgstr "%lu (»%s«) rapporteret af »%s«.\n"

#: wcddir.c:131
msgid "access denied.\n"
msgstr "adgang nægtet.\n"

#: wcddir.c:206
#, c-format
msgid "Searching for shared directories on server %s\n"
msgstr "Søger efter delte mapper på serveren %s\n"

#: wcddir.c:218
#, c-format
msgid "Found %lu shared directories on server %s\n"
msgstr "Fandt %lu delte mapper på serveren %s\n"

#: wcddir.c:335
msgid "Unable to get current working directory: "
msgstr "Kunne ikke indhente nuværende arbejdsmappe: "

#: wcddir.c:362
#, c-format
msgid "Unable to change to directory %s: "
msgstr "Kunne ikke ændre til mappen %s: "

#: wcddir.c:392
#, c-format
msgid "Unable to create directory %s: "
msgstr "Kunne ikke oprette mappen %s: "

#: wcddir.c:420
#, c-format
msgid "Unable to remove directory %s: "
msgstr "Kunne ikke fjerne mappen %s: "

#: wcddir.c:543 wcddir.c:554
#, c-format
msgid "Unable to create directory %s: %s\n"
msgstr "Kunne ikke oprette mappen %s: %s\n"

#: wcddir.c:660
#, c-format
msgid "Unable to get current working directory: %s\n"
msgstr "Kunne ikke indhente nuværende arbejdsmappe: %s\n"

#: wcddir.c:674
#, c-format
msgid "Unable to change to directory %s: %s\n"
msgstr "Kunne ikke ændre til mappen %s: %s\n"

#: wcddir.c:683
#, c-format
msgid "Unable to remove directory %s: %s\n"
msgstr "Kunne ikke fjerne mappen %s: %s\n"

#: stack.c:101
msgid "Error parsing stack\n"
msgstr "Der opstod en fejl under fortolkning af stak\n"

#: display.c:263
msgid "internal error in maxLength(), list == NULL\n"
msgstr "intern fejl i maxLength(), liste == NUL\n"

#: display.c:284
msgid "internal error in maxLengthStack(), s == NULL\n"
msgstr "intern fejl i maxLengthStack(), s == NUL\n"

#: display.c:524 display.c:1150 display.c:1538
#, c-format
msgid "Perfect match for %d directories."
msgstr "Perfekt match for %d mapper."

#: display.c:526 display.c:1152 display.c:1540
#, c-format
msgid "Wild match for %d directories."
msgstr "Jokertegnsmatch for %d mapper."

#: display.c:530 display.c:1164 display.c:1542 display.c:1585
#, c-format
msgid "Please choose one (<Enter> to abort): "
msgstr "Vælg venligst en (<Retur> for at afbryde): "

#: display.c:532 display.c:608 display.c:640 display.c:1157
#, c-format
msgid " w=up x=down ?=help  Page %d/%d "
msgstr " w=op x=ned ?=hjælp Side %d/%d "

#: display.c:684 display.c:1180
msgid "Screenheight must be > 20 for help."
msgstr "Skærmhøjde skal være > 20 for hjælp."

#: display.c:689 display.c:1183
msgid "w or <Up>         page up"
msgstr "w eller <Op>          side op"

#: display.c:690 display.c:1184
msgid "x or z or <Down>  page down"
msgstr "x eller z eller <Ned> side ned"

#: display.c:691 display.c:1185
msgid ", or <Left>       scroll 1 left"
msgstr ", eller <Venstre>     rul 1 mod venstre"

#: display.c:692 display.c:1186
msgid ". or <Right>      scroll 1 right"
msgstr ". eller <Højre>       rul 1 mod højre"

#: display.c:693 display.c:1187
msgid "< or [            scroll 10 left"
msgstr "< eller [             rul 10 mod venstre"

#: display.c:694 display.c:1188
msgid "> or ]            scroll 10 right"
msgstr "> eller ]             rul 10 mod højre"

#: display.c:695 display.c:1189
msgid "CTRL-a or <HOME>  scroll to beginning"
msgstr "CTRL-a eller <HOME>   rul til begyndelsen"

#: display.c:696 display.c:1190
msgid "CTRL-e or <END>   scroll to end"
msgstr "CTRL-e eller <END>    rul til slutningen"

#: display.c:697 display.c:1192
msgid "CTRL-c or <Esc>   abort"
msgstr "CTRL-c eller <Esc>    afbryd"

#: display.c:698 display.c:1193
msgid "<Enter>           abort"
msgstr "<Retur>               afbryd"

#: display.c:699
msgid "Type w or x to quit help."
msgstr "Tast w eller x for at afslutte hjælpeteksten."

#: display.c:1191 graphics.c:1994
msgid "CTRL-l or F5      redraw screen"
msgstr "CTRL-l eller F5       gentegn skærm"

#: display.c:1194 graphics.c:1979 graphics.c:1996 graphics.c:2019
#: graphics.c:2036
msgid "Press any key."
msgstr "Tryk på en tast."

#: display.c:1256 graphics.c:2289
msgid "Error opening terminal, falling back to stdout interface.\n"
msgstr "Der opstod en fejl under åbning af skærm, anvender standardudgrænsefladen.\n"

#: display.c:1277
msgid "screen height must be larger than 3 lines.\n"
msgstr "skærmhøjde skal være større end 3 linjer.\n"

#: display.c:1318 graphics.c:2314
msgid "error creating scroll window.\n"
msgstr "der opstod en fejl under oprettelse af rullevinduet.\n"

#: display.c:1334 graphics.c:2330
msgid "error creating input window.\n"
msgstr "der opstod en fejl under oprettelse af inddatavinduet.\n"

#: graphics.c:290
#, c-format
msgid "Wcd: error: path too long"
msgstr "Wcd: fejl: stien er for lang"

#: graphics.c:1940
msgid "/ = search forward,  ? = search backward,  : = help"
msgstr "/ = søg fremad, ? = søg baglæns, : = hjælp"

#: graphics.c:1945
msgid "SEARCH: "
msgstr "SØG: "

#: graphics.c:1948
msgid "Search: "
msgstr "Søg: "

#: graphics.c:1964
msgid "NAVIGATION MODE (1/2):"
msgstr "NAVIGATIONSTILSTAND (1/2):"

#: graphics.c:1965
msgid "h or <Left>       go left"
msgstr "h eller <venstre>     gå mod venstre"

#: graphics.c:1966
msgid "j or <Down>       go down"
msgstr "j eller <Ned>         gå nedad"

#: graphics.c:1967
msgid "k or <Up>         go up"
msgstr "k elle <Op>           gå op"

#: graphics.c:1968
msgid "l or <Right>      go right"
msgstr "l eller <Højre>       gå mod højre"

#: graphics.c:1969
msgid "* or v or <Space> go forward to dir with same name"
msgstr "* eller v eller <Mellemrum> gå fremad til mappen med det samme navn"

#: graphics.c:1970
msgid "# or p or <BS>    go backward to dir with same name"
msgstr "# eller p eller <BS>  gå tilbage til mappen med det samme navn"

#: graphics.c:1971
msgid "^ or a            go to beginning of line"
msgstr "^ eller a             gå til linjens begyndelse"

#: graphics.c:1972
msgid "$ or e            go to end of line"
msgstr "$ eller e             gå til linjens slutning"

#: graphics.c:1973
msgid "1                 go to root dir"
msgstr "1                     gå til rodmappen"

#: graphics.c:1974
msgid "g or G            go to last dir"
msgstr "g eller G             gå til sidste mappe"

#: graphics.c:1975
msgid "f                 go page forward"
msgstr "f                     gå en side frem"

#: graphics.c:1976
msgid "b                 go page backward"
msgstr "b                     gå en side baglæns"

#: graphics.c:1977
msgid "u                 go half page up"
msgstr "u                     gå en halv side op"

#: graphics.c:1978
msgid "d                 go half page down"
msgstr "d                     gå en halv side ned"

#: graphics.c:1985
msgid "NAVIGATION MODE (2/2):"
msgstr "NAVIGATIONSTILSTAND (2/2):"

#: graphics.c:1986
msgid "A                 switch alternative tree navigation on/off"
msgstr "A                     aktiver/deaktiver alternativ trænavigation"

#: graphics.c:1987
msgid "t                 switch centered mode on/off"
msgstr "t                     aktiver/deaktiver centreret tilstand"

#: graphics.c:1988
msgid "T                 toggle between line drawing and ASCII characters"
msgstr "T                     skift mellem linjetegning og ASCII-tegn"

#: graphics.c:1989
msgid "m                 toggle between compact and wide tree"
msgstr "m                     skift mellem kompakt og bredt træ"

#: graphics.c:1990
msgid "<Esc> or q        abort"
msgstr "<Esc> eller q         afbryd"

#: graphics.c:1991
msgid "/                 search forward"
msgstr "/                     søg fremad"

#: graphics.c:1992
msgid "?                 search backward"
msgstr "?                     søg baglæns"

#: graphics.c:1993
msgid "n                 repeat last / or ? search"
msgstr "n                     gentag sidste søgning / eller ?-søgning"

#: graphics.c:1995 graphics.c:2018
msgid "<Enter>           select directory"
msgstr "<Retur>               vælg mappe"

#: graphics.c:2002
msgid "SEARCH MODE with wildcard and subdir support:"
msgstr "SØGETILSTAND med understøttelse af jokertegn og undermappe:"

#: graphics.c:2003
msgid "<Left>            go left"
msgstr "<Venstre>             gå mod venstre"

#: graphics.c:2004
msgid "<Down>            go down"
msgstr "<Ned>                 gå ned"

#: graphics.c:2005
msgid "<Up>              go up"
msgstr "<Op>                  gå op"

#: graphics.c:2006
msgid "<Right>           go right"
msgstr "<Højre>               gå mod højre"

#: graphics.c:2007
msgid "CTRL-v            go forward to dir with same name"
msgstr "CTRL-v                gå fremad til mappen med samme navn"

#: graphics.c:2008
msgid "CTRL-p            go backward to dir with same name"
msgstr "CTRL-p                gå baglæns til mappen med samme navn"

#: graphics.c:2009
msgid "CTRL-a            go to beginning of line"
msgstr "CTRL-a                gå til begyndelsen af linjen"

#: graphics.c:2010
msgid "CTRL-e            go to end of line"
msgstr "CTRL-e                gå til slutningen af linjen"

#: graphics.c:2011
msgid "CTRL-g            go to last dir"
msgstr "CTRL-g                gå til sidste mappe"

#: graphics.c:2012
msgid "CTRL-f            go page forward"
msgstr "CTRL-f                gå 1 side frem"

#: graphics.c:2013
msgid "CTRL-b            go page backward"
msgstr "CTRL-b                gå 1 side baglæns"

#: graphics.c:2014
msgid "CTRL-u            go half page up"
msgstr "CTRL-u                gå en halv side op"

#: graphics.c:2015
msgid "CTRL-d            go half page down"
msgstr "CTRL-d                gå en halv side ned"

#: graphics.c:2016
msgid "<Esc> or CTRL-x   abort SEARCH MODE"
msgstr "<Esc> eller CTRL-x    afbryd SØGETILSTAND"

#: graphics.c:2017
msgid "CTRL-n            repeat last / or ? search"
msgstr "CTRL-n                gentag sidste søgning eller ?-søgning"

#: graphics.c:2025
msgid "ZOOMING:"
msgstr "ZOOM:"

#: graphics.c:2026
msgid "z or i or CTRL-i  zoom in"
msgstr "z eller i eller CTRL-i  zoom ind"

#: graphics.c:2027
msgid "Z or o or CTRL-o  zoom out"
msgstr "Z eller o eller CTRL-o  zoom ud"

#: graphics.c:2028
msgid "c                 condense: fold current level"
msgstr "c                     kondensere: fold nuværende niveau"

#: graphics.c:2029
msgid "C                 condense: fold subdir level"
msgstr "C                     kondensere: fold undermappeniveau"

#: graphics.c:2030
msgid "w                 condense: fold current and subdir levels"
msgstr ""
"w                     kondensere: fold nuværende niveau og\n"
"                                  undermappeniveauer"

#: graphics.c:2031
msgid "y or CTRL-y       uncondense: unfold current and subdir levels"
msgstr ""
"y eller CTRL-y        afkondensere: fold nuværende niveau og og\n"
"                                    undermappeniveauer ud"

#: graphics.c:2032
msgid "r or CTRL-r       uncondense: unfold all directories"
msgstr "r eller CTRL-r        afkondensere: fold alle mapper ud"

#: graphics.c:2033
msgid "-                 fold directory"
msgstr "-                     fold mappe"

#: graphics.c:2034
msgid "+ or =            unfold directory"
msgstr "+ eller =             fold mappe ud"

#: graphics.c:2035
msgid "l or <Right>      unfold and go right"
msgstr "l eller <Right>       fold ud og gå mod højre"

#: graphics.c:2039
msgid "Screenheight must be > 21 for help."
msgstr "Skærmhøjde skal være > 21 for hjælp."

#: graphics.c:2271 graphics.c:2757
#, c-format
msgid "Cannot find the current path %s in the directory tree.\n"
msgstr "Kan ikke finde den nuværende sti %s i mappetræet.\n"

#: finddirs.c:241 finddirs.c:249 finddirs.c:273 finddirs.c:300 finddirs.c:314
#, c-format
msgid "Unable to remove file %s: %s\n"
msgstr "Kan ikke fjerne filen %s: %s\n"

#: finddirs.c:322 finddirs.c:486
#, c-format
msgid "Unable to close directory %s: %s\n"
msgstr "Kan ikke lukke mappen %s: %s\n"

#: finddirs.c:365
#, c-format
msgid "finddirs(): can't determine path in directory %s\n"
msgstr "finddirs(): kan ikke bestemme sti i mappen %s\n"

#: finddirs.c:366
msgid "path probably too long.\n"
msgstr "sti er sandsynligvis for lang.\n"

#: c3po/Error.c:41
#, c-format
msgid "in '%s', insufficient memory for allocation\n"
msgstr "i »%s«, utilstrækkelig hukommelse for allokering\n"
